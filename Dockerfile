FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app

ENV ASPNETCORE_URLS http://+:2020

EXPOSE 2020

COPY app .

ENTRYPOINT ["dotnet", "DaysCalculator.dll"]