﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DaysCalculator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DaysCalculatorController : ControllerBase
    {
        [HttpGet]
        public ActionResult<int> GetDays(DateTime? dtfrom, DateTime? dtto)
        {
            var from = dtfrom.HasValue ? dtfrom.Value : DateTime.Now;
            var to = dtto.HasValue ? dtto.Value : DateTime.Now;
            return DaysCalculatorHelper.CalculateDays(from,to);
        }

    }
}
