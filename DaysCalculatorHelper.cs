﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DaysCalculator
{
    public static class DaysCalculatorHelper
    {
        public static int CalculateDays(DateTime dtFrom,DateTime dtTo)
        {
            return (dtTo - dtFrom).Days;
        }
    }
}
