﻿## 应用概述
以.net core实现一个简单的api为基础，来学习实现一个简单CI/CD流程。  
采用gitlab+aws+docker，实现了该项目的代码管理， 持续编译，部署。
## 流水线阶段描述
### 编译
1. 使用gitlab提供的ci功能，编辑.gitlab-ci.yml文件，写出本工程的编译命令。
2. 在aws提供的机器上安装gitlab-runner，执行编译。

### 部署
1. 将前面编译的结果文件，拷贝到docker中。
2. 启动docker容器。

## 工具链描述
1. gitlab
    * 代码管理。
    * .gitlab-ci.yml文件，流水线命令管理。
2. aws
    * docker，部署容器。
    * gitlab-runner，流水线命令执行。

## CI/CD 功能
1. 自动编译
2. 自动部署

## 演示说明
gitlab: https://gitlab.com/jd83/DevOpsChinaStudy2020/-/pipelines  
api: http://68.79.40.245:2020/swagger/index.html